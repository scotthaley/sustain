import * as auth from './auth/actions';
import * as groceries from './groceries/actions';
import * as recipes from './recipes/actions';

export default { recipes, groceries, auth };
