import { combineReducers } from 'redux';
import { auth } from './auth/reducers';
import { groceries } from './groceries/reducers';
import { recipes } from './recipes/reducers';

export default combineReducers({
  recipes,
  groceries,
  auth
});
