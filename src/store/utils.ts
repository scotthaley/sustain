import { compose } from 'redux';
import { _DeepReadonlyArray } from 'utility-types/dist/mapped-types';

export const composeEnhancers =
  (process.env.NODE_ENV === 'development' &&
    window &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

export const titleCase = (str: string) =>
  str
    .toLowerCase()
    .replace('-', ' ')
    .split(' ')
    .map(word => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' ');

export const kebabCase = (str: string) => str.toLowerCase().replace(/ /g, '-');

export const moveInArray = (
  array: _DeepReadonlyArray<any>,
  from: number,
  to: number
) => {
  const returnArray = new Array(...array);
  returnArray.splice(to, 0, returnArray.splice(from, 1)[0]);
  return returnArray;
};

export const removeInArray = (
  array: _DeepReadonlyArray<any>,
  index: number
) => {
  const returnArray = new Array(...array);
  returnArray.splice(index, 1);
  return returnArray;
};
