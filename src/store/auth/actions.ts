import * as firebase from 'firebase/app';
import { createAction, createAsyncAction } from 'typesafe-actions';
import { IDefaults, ISyncUserPayload } from '../models';
import * as ActionTypes from './action-types';

export const signIn = createAsyncAction(
  ActionTypes.SIGN_IN,
  ActionTypes.SIGN_IN_SUCCESS,
  ActionTypes.SIGN_IN_FAILURE
)<
  {
    email: string;
    password: string;
  },
  { email: string },
  string
>();

export const signUp = createAsyncAction(
  ActionTypes.SIGN_UP,
  ActionTypes.SIGN_UP_SUCCESS,
  ActionTypes.SIGN_UP_FAILURE
)<
  {
    email: string;
    password: string;
  },
  firebase.auth.UserCredential,
  string
>();

export const storeUser = createAction(
  ActionTypes.STORE_USER
)<firebase.User | null>();

export const setGroceryItemDefault = createAction(
  ActionTypes.SET_GROCERY_ITEM_DEFAULT
)<{
  name: string;
  aisle: string;
  measurement: number;
  quantity: number;
}>();

export const syncUserUp = createAction(ActionTypes.SYNC_UP)();
export const syncUserDown = createAction(ActionTypes.SYNC_DOWN)<
  ISyncUserPayload
>();
export const syncDefaults = createAction(ActionTypes.SYNC_DEFAULTS)<
  IDefaults
>();
