import * as firebase from 'firebase/app';
import { createReducer } from 'typesafe-actions';
import { DeepReadonly } from 'utility-types';
import { IDefaults } from '../models';
import { kebabCase } from '../utils';
import * as Actions from './actions';

export type AuthState = DeepReadonly<{
  user: firebase.User | null;
  defaults: IDefaults;
  userDefaults: IDefaults;
}>;
const initialState: AuthState = {
  user: null,
  defaults: {
    groceryItems: {}
  },
  userDefaults: {
    groceryItems: {}
  }
};

export const auth = createReducer(initialState)
  .handleAction(Actions.storeUser, (state, action) => ({
    ...state,
    user: action.payload
  }))
  .handleAction(Actions.setGroceryItemDefault, (state, action) => ({
    ...state,
    userDefaults: {
      ...state.userDefaults,
      groceryItems: {
        ...state.userDefaults.groceryItems,
        [kebabCase(action.payload.name)]: {
          aisle: action.payload.aisle,
          measurement: action.payload.measurement,
          quantity: action.payload.quantity
        }
      }
    }
  }))
  .handleAction(Actions.syncUserDown, (state, action) => ({
    ...state,
    userDefaults: action.payload.defaults
  }))
  .handleAction(Actions.syncDefaults, (state, action) => ({
    ...state,
    defaults: action.payload
  }));
