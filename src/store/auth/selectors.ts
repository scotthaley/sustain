import { RootState } from 'typesafe-actions';
import { IGroceryItem } from '../models';
import { titleCase } from '../utils';

export const getUserStore = (state: RootState) => state.auth;
export const getUser = (state: RootState) => getUserStore(state).user;

export const getDefaults = (state: RootState) => getUserStore(state).defaults;
export const getUserDefaults = (state: RootState) =>
  getUserStore(state).userDefaults;
export const getDefaultGroceryItems = (state: RootState) => ({
  ...getDefaults(state).groceryItems,
  ...getUserDefaults(state).groceryItems
});

export const queryGroceryItems = (query: string) => (
  state: RootState
): IGroceryItem[] => {
  const groceryItems = getDefaultGroceryItems(state);
  return Object.keys(groceryItems)
    .filter(
      itemName =>
        titleCase(itemName)
          .toLowerCase()
          .indexOf(query.toLowerCase()) !== -1
    )
    .map(itemName => ({
      name: titleCase(itemName),
      ...groceryItems[itemName],
      checked: false
    }));
};
