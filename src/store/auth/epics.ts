import { Epic } from 'redux-observable';
import { from, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  ignoreElements,
  map,
  switchMap,
  take,
  tap
} from 'rxjs/operators';
import { isActionOf, RootAction, RootState, Services } from 'typesafe-actions';
import * as GroceryActions from '../groceries/actions';
import * as Actions from './actions';

export const signinEpic: Epic<RootAction, RootAction, RootState, Services> = (
  action$,
  state$,
  { api }
) =>
  action$.pipe(
    filter(isActionOf(Actions.signIn.request)),
    switchMap(action =>
      from(
        api.auth.emailAuth(action.payload.email, action.payload.password)
      ).pipe(
        map(() => Actions.signIn.success({ email: action.payload.email })),
        catchError((message: string) => of(Actions.signIn.failure(message)))
      )
    )
  );

export const signupEpic: Epic<RootAction, RootAction, RootState, Services> = (
  action$,
  state$,
  { api }
) =>
  action$.pipe(
    filter(isActionOf(Actions.signUp.request)),
    switchMap(action =>
      from(
        api.auth.emailSignUp(action.payload.email, action.payload.password)
      ).pipe(
        map(userCred => Actions.signUp.success(userCred)),
        catchError((message: string) => of(Actions.signUp.failure(message)))
      )
    )
  );

export const signupSuccessEpic: Epic<
  RootAction,
  RootAction,
  RootState,
  Services
> = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(Actions.signUp.success)),
    tap(action => {
      if (action.payload.user && action.payload.user.email) {
        api.auth.createUserDoc(
          action.payload.user.uid,
          action.payload.user.email
        );
      }
    }),
    ignoreElements()
  );

export const watchSyncEpic: Epic<RootAction, RootAction> = action$ =>
  action$.pipe(
    filter(
      action =>
        [
          ...Object.values(GroceryActions),
          Actions.setGroceryItemDefault
        ].find(a => isActionOf(a)(action)) !== undefined
    ),
    map(() => Actions.syncUserUp())
  );

export const syncUserEpic: Epic<RootAction, RootAction, RootState, Services> = (
  action$,
  state$,
  { api }
) =>
  action$.pipe(
    filter(isActionOf(Actions.syncUserUp)),
    debounceTime(250),
    tap(() => {
      state$.pipe(take(1)).subscribe(state => api.auth.sync(state));
    }),
    ignoreElements()
  );
