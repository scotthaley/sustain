export const SIGN_IN = 'sign_in';
export const SIGN_IN_SUCCESS = 'sign_in_success';
export const SIGN_IN_FAILURE = 'sign_in_failure';

export const SIGN_UP = 'sign_up';
export const SIGN_UP_SUCCESS = 'sign_up_success';
export const SIGN_UP_FAILURE = 'sign_up_failure';

export const STORE_USER = 'store_user';

export const SET_GROCERY_ITEM_DEFAULT = 'set_grocery_item_default';

export const SYNC_UP = 'sync_up';
export const SYNC_DOWN = 'sync_down';
export const SYNC_DEFAULTS = 'sync_defaults';
