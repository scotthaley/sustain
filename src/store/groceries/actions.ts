import { createAction } from 'typesafe-actions';
import { IGroceryItem } from '../models';
import * as ActionTypes from './action-types';

export const newAisle = createAction(ActionTypes.NEW_AISLE)<string>();

export const reorderAisle = createAction(ActionTypes.REORDER_AISLE)<{
  origIndex: number;
  newIndex: number;
}>();

export const deleteAisle = createAction(ActionTypes.DELETE_AISLE)<number>();

export const newItem = createAction(ActionTypes.NEW_ITEM)<IGroceryItem>();
export const deleteItem = createAction(ActionTypes.DELETE_ITEM)<string>();
export const updateItem = createAction(ActionTypes.UPDATE_ITEM)<{
  name: string;
  item: IGroceryItem;
}>();
export const checkItem = createAction(ActionTypes.CHECK_ITEM)<{
  name: string;
  checked: boolean;
}>();
