import { createReducer } from 'typesafe-actions';
import { DeepReadonly } from 'utility-types';
import { syncUserDown } from '../auth/actions';
import { IGroceryItem } from '../models';
import { moveInArray, removeInArray, titleCase } from '../utils';
import * as Actions from './actions';

export type GroceriesState = DeepReadonly<{
  aisles: string[];
  items: IGroceryItem[];
}>;
const initialState: GroceriesState = {
  aisles: [],
  items: []
};

export const groceries = createReducer(initialState)
  .handleAction(Actions.newAisle, (state, action) => ({
    ...state,
    aisles: [...state.aisles, titleCase(action.payload)]
  }))
  .handleAction(Actions.reorderAisle, (state, action) => ({
    ...state,
    aisles: moveInArray(
      state.aisles,
      action.payload.origIndex,
      action.payload.newIndex
    )
  }))
  .handleAction(Actions.deleteAisle, (state, action) => ({
    ...state,
    aisles: removeInArray(state.aisles, action.payload)
  }))
  .handleAction(Actions.newItem, (state, action) => ({
    ...state,
    items: [
      ...state.items.filter(item => item.name !== action.payload.name),
      action.payload
    ]
  }))
  .handleAction(Actions.deleteItem, (state, action) => ({
    ...state,
    items: state.items.filter(item => item.name !== action.payload)
  }))
  .handleAction(Actions.checkItem, (state, action) => ({
    ...state,
    items: state.items.map(item =>
      item.name === action.payload.name
        ? { ...item, checked: action.payload.checked }
        : item
    )
  }))
  .handleAction(Actions.updateItem, (state, action) => ({
    ...state,
    items: state.items.map(item =>
      item.name === action.payload.name
        ? { ...item, ...action.payload.item }
        : item
    )
  }))

  .handleAction(syncUserDown, (state, action) => ({
    ...state,
    aisles: action.payload.groceries.aisles,
    items: action.payload.groceries.items
  }));
