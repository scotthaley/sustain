import { Epic } from 'redux-observable';
import { filter, map } from 'rxjs/operators';
import { isActionOf, RootAction } from 'typesafe-actions';
import * as AuthActions from '../auth/actions';
import * as Actions from './actions';

export const newItemEpic: Epic<RootAction, RootAction> = action$ =>
  action$.pipe(
    filter(isActionOf(Actions.newItem)),
    map(action =>
      AuthActions.setGroceryItemDefault({
        name: action.payload.name,
        aisle: action.payload.aisle,
        measurement: action.payload.measurement,
        quantity: action.payload.quantity
      })
    )
  );
