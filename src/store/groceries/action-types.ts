export const NEW_AISLE = 'new_aisle';
export const REORDER_AISLE = 'reorder_aisle';
export const DELETE_AISLE = 'delete_aisle';

export const NEW_ITEM = 'new_item';
export const DELETE_ITEM = 'delete_item';
export const UPDATE_ITEM = 'update_item';
export const CHECK_ITEM = 'check_item';
