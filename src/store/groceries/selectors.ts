import { RootState } from 'typesafe-actions';

export const getGroceriesStore = (state: RootState) => state.groceries;
export const getAisles = (state: RootState) => getGroceriesStore(state).aisles;
export const getItems = (state: RootState) => getGroceriesStore(state).items;
export const getItem = (name: string | undefined) => (state: RootState) =>
  name ? getItems(state).find(item => item.name === name) : null;
