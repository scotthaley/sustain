import { RootState } from 'typesafe-actions';

export const getRecipesStore = (state: RootState) => state.recipes;
