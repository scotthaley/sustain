import { DeepReadonly } from 'utility-types';
import { IRecipe } from '../models';
import { createReducer } from 'typesafe-actions';
import * as Actions from './actions';

export type RecipesState = DeepReadonly<IRecipe[]>;
const initialState: RecipesState = [];

export const recipes = createReducer(initialState).handleAction(
  Actions.syncRecipesDown,
  (state, action) => action.payload
);
