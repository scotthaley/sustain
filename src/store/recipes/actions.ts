import { createAction } from 'typesafe-actions';
import * as ActionTypes from './action-types';
import { IRecipe } from '../models';

export const syncRecipesDown = createAction(ActionTypes.SYNC_RECIPES_DOWN)<
  IRecipe[]
>();
