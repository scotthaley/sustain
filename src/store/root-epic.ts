import { combineEpics } from 'redux-observable';
import * as authEpics from './auth/epics';
import * as groceryEpics from './groceries/epics';

export default combineEpics(
  ...Object.values(authEpics),
  ...Object.values(groceryEpics)
);
