export enum Measurement {
  Unit,
  Teaspoon,
  Tablespoon,
  FluidOunce,
  Cup,
  Pint,
  Quart,
  Gallon,
  Ml,
  L,
  Dl,
  Pound,
  Ounce,
  Mg,
  G,
  Kg,
  Mm,
  Cm,
  M,
  Inch
}

export enum MeasurementType {
  Single,
  Volume,
  Mass,
  Length
}

export interface ICategory {
  label: string;
  id: number;
}

export interface IRecipe {
  id: string;
  name: string;
  img: string;
}

export interface IGroceryItem {
  name: string;
  quantity: number;
  measurement: Measurement;
  aisle: string;
  checked: boolean;
  recipe?: string;
}

export interface IGroceryItemDefault {
  quantity: number;
  measurement: Measurement;
  aisle: string;
}

export interface ISyncUserPayload {
  groceries: {
    aisles: string[];
    items: IGroceryItem[];
  };
  defaults: {
    groceryItems: {
      [name: string]: {
        aisle: string;
        measurement: number;
        quantity: number;
      };
    };
  };
}

export interface IDefaults {
  groceryItems: { [name: string]: IGroceryItemDefault };
}
