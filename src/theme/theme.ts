const colors = {
  bg0: '#111217',
  bg1: '#222831',
  bg2: '#393E46',
  bg3: '#6E7888',
  fg1: '#EEEEEE',
  fg2: '#61FF91',
  fg3: '#c8c7cc',
  text: {
    primary: '#EEEEEE',
    secondary: '#A3F7BF',
    light: '#6E7888',
    dark: '#111217'
  },
  primary: '#29A19C'
};
const zIndex = [
  'base',
  'sidenavscrim',
  'sidenav',
  'itempicker',
  'categorypicker'
];
export const theme = {
  colors,
  space: [0, 4, 8, 16, 32, 64],
  shadows: {
    card: '0px 1px 15px 0px rgba(0,0,0,0.3)',
    inset: 'inset 0px 0px 10px 0px rgba(0,0,0,0.3)'
  },
  forms: {
    input: {
      color: 'text.primary',
      backgroundColor: 'bg3',
      border: 'none',
      borderRadius: '8px',
      width: '70%'
    }
  },
  variants: {
    floating: {
      boxShadow: 'card'
    },
    card: {
      boxShadow: 'card',
      borderRadius: '10px',
      backgroundColor: colors.bg2,
      overflow: 'hidden'
    },
    bottomBorder: {
      borderBottom: `solid 0.55px ${colors.fg3}`
    },
    overflowEllipsis: {
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    }
  },
  zIndices: zIndex.reduce((acc, cur, i) => ({ ...acc, [cur]: i }), {})
};
