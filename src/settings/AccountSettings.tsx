import * as firebase from 'firebase/app';
import React from 'react';
import { Flex, Text } from 'rebass';
import ProfileImage, {
  ProfileImageSize
} from '../components/atoms/ProfileImage';
import SettingsButton from '../components/atoms/SettingsButton';
import SettingsCard from '../components/atoms/SettingsCard';

const AccountSettings: React.FC = () => {
  const logout = () => {
    firebase.auth().signOut();
  };

  return (
    <SettingsCard title="Account">
      <Flex flexDirection="column" alignItems="center">
        <Flex alignItems="center" style={{ maxWidth: '100%' }} p={3}>
          <ProfileImage
            email="williamscotthaley@gmail.com"
            size={ProfileImageSize.Medium}
          ></ProfileImage>
          <Flex flexDirection="column">
            <Text ml={3} color="text.dark" fontSize={20}>
              Scott Haley
            </Text>
            <Text
              ml={3}
              color="text.light"
              fontSize={14}
              style={{
                overflow: 'hidden',
                whiteSpace: 'nowrap',
                textOverflow: 'ellipsis'
              }}
            >
              williamscotthaley@gmail.com
            </Text>
          </Flex>
        </Flex>
        <SettingsButton onClick={logout}>
          Sign out of this account
        </SettingsButton>
      </Flex>
    </SettingsCard>
  );
};

export default AccountSettings;
