import { arrowForward } from 'ionicons/icons';
import React from 'react';
import { useHistory } from 'react-router';
import SettingsButton from '../components/atoms/SettingsButton';
import SettingsCard from '../components/atoms/SettingsCard';

const GrocerySettings: React.FC = () => {
  const history = useHistory();

  return (
    <SettingsCard title="Groceries">
      <SettingsButton
        top={true}
        icon={arrowForward}
        onClick={() => history.push('/settings/aisles')}
      >
        Edit grocery aisles
      </SettingsButton>
      <SettingsButton icon={arrowForward}>Grocery translations</SettingsButton>
    </SettingsCard>
  );
};

export default GrocerySettings;
