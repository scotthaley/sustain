import { Measurement } from '../store/models';
import { getMeasurementByString, measurementRegex } from './measurement-utils';

export const processItemString = (
  itemString: string
): { name: string; measurement?: Measurement; quantity?: number } => {
  const quantity = processQuantity(itemString);
  const measurement = processMeasurement(itemString);
  const name = processName(itemString);
  return {
    name,
    measurement,
    quantity
  };
};

const processQuantity = (itemString: string): number | undefined => {
  const match = itemString.toLowerCase().match(/[a-z ]*(\d*).*/);
  if (match) {
    return Number(match[1]);
  }
};

const processMeasurement = (itemString: string): Measurement | undefined => {
  const match = itemString.match(measurementRegex);
  if (match) {
    return getMeasurementByString(match[1]);
  }
};

const processName = (itemString: string): string => {
  return itemString
    .replace(measurementRegex, '')
    .replace(new RegExp(/\d/, 'g'), '')
    .trim()
    .toLowerCase();
};
