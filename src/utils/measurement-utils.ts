import { Measurement, MeasurementType } from '../store/models';

const measurementStrings = [
  'fl oz',
  'fl. oz.',
  'oz',
  'ounce',
  'tsp',
  'teaspoon',
  'tbsp',
  'tablespoon',
  'cup',
  'pint',
  'quart',
  'gallon',
  'ml',
  'l',
  'dl',
  'pound',
  'lb',
  'mg',
  'milligram',
  'g',
  'gram',
  'kg',
  'killigram',
  'mm',
  'millimeter',
  'cm',
  'centimeter',
  'm',
  'meter',
  'inch',
  'in',
  'package',
  'box'
];

export const measurementRegex = new RegExp(
  `[0-9 ]+(${measurementStrings.join('|')})s?( of)? `,
  'i'
);
///(oz|ounce|fl oz|fl\. oz\.|fl)( of)?/i;

export const getMeasurementDisplayName = (
  measurement: Measurement,
  plural: boolean,
  exact: boolean = false
) => {
  switch (measurement) {
    case Measurement.Unit:
      return exact ? 'unit' : '';
    case Measurement.FluidOunce:
      return 'fl oz';
    case Measurement.Ounce:
      return 'oz';
    case Measurement.Gallon:
      return plural ? 'gallons' : 'gallon';
    default:
      return Measurement[measurement].toString().toLowerCase();
  }
};

export const getMeasurementByString = (
  measurementString: string
): Measurement => {
  if (!measurementString) {
    return Measurement.Unit;
  }
  switch (measurementString.toLowerCase()) {
    case 'fl':
    case 'fl oz':
    case 'fl. oz.':
      return Measurement.FluidOunce;
    case 'oz':
    case 'ounce':
      return Measurement.Ounce;
    case 'tsp':
    case 'teaspoon':
      return Measurement.Teaspoon;
    case 'tbsp':
    case 'tablespoon':
      return Measurement.Tablespoon;
    case 'cup':
      return Measurement.Cup;
    case 'pint':
      return Measurement.Pint;
    case 'quart':
      return Measurement.Quart;
    case 'gallon':
      return Measurement.Gallon;
    case 'ml':
      return Measurement.Ml;
    case 'l':
      return Measurement.L;
    case 'dl':
      return Measurement.Dl;
    case 'pound':
    case 'lb':
      return Measurement.Pound;
    case 'mg':
    case 'milligram':
      return Measurement.Mg;
    case 'g':
    case 'gram':
      return Measurement.G;
    case 'kg':
    case 'killigram':
      return Measurement.Kg;
    case 'mm':
    case 'millimeter':
      return Measurement.Mm;
    case 'cm':
    case 'centimeter':
      return Measurement.Cm;
    case 'm':
    case 'meter':
      return Measurement.M;
    case 'inch':
    case 'in':
      return Measurement.Inch;
    case 'package':
    case 'box':
    default:
      return Measurement.Unit;
  }
};

export const getMeasurementType = (
  measurement: Measurement
): MeasurementType => {
  switch (measurement) {
    case Measurement.Teaspoon:
    case Measurement.Tablespoon:
    case Measurement.FluidOunce:
    case Measurement.Cup:
    case Measurement.Pint:
    case Measurement.Quart:
    case Measurement.Gallon:
    case Measurement.Ml:
    case Measurement.L:
    case Measurement.Dl:
      return MeasurementType.Volume;
    case Measurement.Pound:
    case Measurement.Ounce:
    case Measurement.Mg:
    case Measurement.G:
    case Measurement.Kg:
      return MeasurementType.Mass;
    case Measurement.Mm:
    case Measurement.Cm:
    case Measurement.M:
    case Measurement.Inch:
      return MeasurementType.Length;
    case Measurement.Unit:
      return MeasurementType.Single;
  }
};
