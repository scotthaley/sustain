import { IonButton, IonInput, IonItem, IonLabel } from '@ionic/react';
import { Formik } from 'formik';
import React from 'react';
import { Box } from 'rebass';
import BackButton from '../components/atoms/BackButton';
import BaseContent from '../components/atoms/BaseContent';
import BasePage from '../components/atoms/BasePage';
import SettingsCard from '../components/atoms/SettingsCard';
import HeaderBar from '../components/molecules/HeaderBar';
import QuantityPicker from '../components/molecules/QuantityPicker';
import { useDispatch, useSelector } from 'react-redux';
import { getItem } from '../store/groceries/selectors';
import { useHistory, useParams } from 'react-router';
import { IGroceryItem } from '../store/models';
import { updateItem } from '../store/groceries/actions';

interface IGroceryListItemSettingsProps {
  onMenu?: () => void;
}

const GroceryListItemSettings: React.FC<IGroceryListItemSettingsProps> = ({
  onMenu
}) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { name } = useParams();
  const item = useSelector(getItem(name));

  return (
    <BasePage>
      <HeaderBar title="Grocery List" onMenu={onMenu} />
      <BaseContent>
        <BackButton />
        {item && (
          <SettingsCard title="Edit Item">
            <Box py={3}>
              <Formik
                initialValues={{
                  name: item.name,
                  quantity: item.quantity,
                  measurement: item.measurement,
                  recipe: item.recipe || ''
                }}
                onSubmit={values => {
                  const newItem: IGroceryItem = {
                    ...item,
                    ...values
                  };
                  dispatch(updateItem({ name: item.name, item: newItem }));
                  history.goBack();
                }}
              >
                {({
                  values,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  setFieldValue
                }) => (
                  <form onSubmit={handleSubmit}>
                    <IonItem>
                      <IonLabel position="floating">Name</IonLabel>
                      <IonInput
                        type="text"
                        name="name"
                        onInput={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                      />
                    </IonItem>
                    <IonItem>
                      <IonLabel position="floating">What is this for?</IonLabel>
                      <IonInput
                        type="text"
                        name="recipe"
                        onInput={handleChange}
                        onBlur={handleBlur}
                        value={values.recipe}
                        placeholder="e.g. recipe, party, house stuff"
                      />
                    </IonItem>
                    <Box mt={3}>
                      <IonItem lines="none">
                        <IonLabel position="fixed">Quantity</IonLabel>
                        <QuantityPicker
                          qty={values.quantity}
                          measurement={values.measurement}
                          onChange={(qty, measurement) => {
                            setFieldValue('quantity', qty);
                            setFieldValue('measurement', measurement);
                          }}
                        />
                      </IonItem>
                    </Box>
                    <Box mt={3} pr={3} style={{ textAlign: 'right' }}>
                      <IonButton
                        type="button"
                        color="danger"
                        onClick={() => history.goBack()}
                      >
                        Discard Changes
                      </IonButton>
                      <IonButton type="submit" color="primary">
                        Save
                      </IonButton>
                    </Box>
                  </form>
                )}
              </Formik>
            </Box>
          </SettingsCard>
        )}
      </BaseContent>
    </BasePage>
  );
};

export default GroceryListItemSettings;
