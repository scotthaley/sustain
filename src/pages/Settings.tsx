import React from 'react';
import BaseContent from '../components/atoms/BaseContent';
import BasePage from '../components/atoms/BasePage';
import HeaderBar from '../components/molecules/HeaderBar';
import AccountSettings from '../settings/AccountSettings';
import GrocerySettings from '../settings/GrocerySettings';

interface ISettingsProps {
  onMenu?: () => void;
}

const Settings: React.FC<ISettingsProps> = ({ onMenu }) => {
  return (
    <BasePage>
      <HeaderBar title="Settings" onMenu={onMenu}></HeaderBar>
      <BaseContent>
        <AccountSettings />
        <GrocerySettings />
      </BaseContent>
    </BasePage>
  );
};

export default Settings;
