import React from 'react';
import plus from '../assets/icons/plus.png';
import BasePage from '../components/atoms/BasePage';
import HeaderBar from '../components/molecules/HeaderBar';
import BaseContent from '../components/atoms/BaseContent';
import { Box } from 'rebass';
import RecipeIcon from '../components/atoms/RecipeIcon';
import { useSelector } from 'react-redux';
import { getRecipesStore } from '../store/recipes/selectors';

interface IRecipesProps {
  onMenu?: () => void;
}

const Recipes: React.FC<IRecipesProps> = ({ onMenu }) => {
  const recipes = useSelector(getRecipesStore);

  return (
    <BasePage>
      <HeaderBar title="Recipes" rightIcon={plus} onMenu={onMenu} />
      <BaseContent className="ion-padding">
        <Box p={4}>
          {recipes.map(r => (
            <RecipeIcon key={r.id} name={r.name} icon={r.img} />
          ))}
        </Box>
      </BaseContent>
    </BasePage>
  );
};

export default Recipes;
