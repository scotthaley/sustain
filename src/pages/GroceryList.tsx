import { IonButtons, IonFooter, IonToolbar } from '@ionic/react';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box } from 'rebass';
import plus from '../assets/icons/plus.png';
import BaseContent from '../components/atoms/BaseContent';
import BasePage from '../components/atoms/BasePage';
import VisibilityButton from '../components/atoms/VisibilityButton';
import Card from '../components/molecules/Card';
import CheckboxListItem from '../components/molecules/CheckboxListItem';
import HeaderBar from '../components/molecules/HeaderBar';
import ItemPicker from '../components/molecules/ItemPicker';
import GroceryListDrawer from '../components/organisms/GroceryListDrawer';
import { queryGroceryItems } from '../store/auth/selectors';
import { checkItem, deleteItem, newItem } from '../store/groceries/actions';
import { getAisles, getItems } from '../store/groceries/selectors';
import { IGroceryItem, Measurement } from '../store/models';
import { titleCase } from '../store/utils';
import { processItemString } from '../utils/item-processing';
import { getMeasurementDisplayName } from '../utils/measurement-utils';
import { useHistory } from 'react-router';

interface IGroceryListProps {
  onMenu?: () => void;
}

const GroceryList: React.FC<IGroceryListProps> = ({ onMenu }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const aisles = useSelector(getAisles);
  const items = useSelector(getItems);
  const [selectedCategory, setSelectedCategory] = React.useState(
    'Uncategorized'
  );
  const [drawerExpanded, setDrawerExpanded] = React.useState(false);
  const [itemText, setItemText] = React.useState('');
  const [overrideCategory, setOverrideCategory] = React.useState(false);
  const [checkedVisible, setCheckedVisible] = React.useState(true);
  const processedItem = processItemString(itemText);
  const itemMatches = useSelector(queryGroceryItems(processedItem.name));

  const newItems = () => {
    const mappedItemMatches = itemMatches.map(item => ({
      ...item,
      quantity: processedItem.quantity || item.quantity,
      measurement: processedItem.measurement || item.measurement,
      aisle:
        overrideCategory &&
        processedItem.name.toLowerCase() === item.name.toLowerCase()
          ? selectedCategory
          : item.aisle,
      checked: false
    }));

    if (
      itemMatches.find(
        item => item.name.toLowerCase() === processedItem.name.toLowerCase()
      )
    ) {
      return mappedItemMatches;
    }
    return [
      {
        name: titleCase(processedItem.name),
        quantity: processedItem.quantity || 1,
        measurement: processedItem.measurement || 0,
        aisle: selectedCategory,
        checked: false
      },
      ...mappedItemMatches
    ];
  };

  const newItemList = newItems();

  const handleItemSelect = (text: string, aisle: string) => {
    setItemText(text);
    setSelectedCategory(aisle);
  };

  const handleItemAdd = (item?: IGroceryItem) => {
    setItemText('');
    dispatch(newItem(item || newItemList[0]));
  };

  const handleItemDelete = (name: string) => {
    dispatch(deleteItem(name));
  };

  const handleItemChecked = (name: string, checked: boolean) => {
    dispatch(checkItem({ name, checked }));
  };

  const showItemPicker = drawerExpanded && itemText.length > 0;

  return (
    <BasePage>
      <HeaderBar
        title="Grocery List"
        rightIcon={plus}
        onMenu={onMenu}
        onRightAction={() => setDrawerExpanded(!drawerExpanded)}
      />
      <GroceryListDrawer
        expanded={drawerExpanded}
        requestedAisle={newItemList[0] ? newItemList[0].aisle : undefined}
        itemText={itemText}
        onItemText={setItemText}
        onAisle={setSelectedCategory}
        onAisleOverride={setOverrideCategory}
        onItemAdd={handleItemAdd}
      />
      <BaseContent className="ion-padding">
        {showItemPicker && (
          <ItemPicker
            items={newItemList}
            onItemSelect={handleItemSelect}
            onItemAdd={handleItemAdd}
          />
        )}
        {!showItemPicker &&
          aisles
            .filter(a => items.find(item => item.aisle === a))
            .map(a => (
              <Card
                title={a}
                key={a}
                hidden={
                  !items.find(
                    item =>
                      item.aisle === a && (checkedVisible || !item.checked)
                  )
                }
                maxHeight={
                  (items.filter(item => item.aisle === a).length + 1) * 55
                }
              >
                {items
                  .filter(item => item.aisle === a)
                  .map(item => (
                    <CheckboxListItem
                      key={item.name}
                      title={item.name}
                      qty={
                        item.quantity > 1 ||
                        item.measurement !== Measurement.Unit
                          ? `${item.quantity} ${getMeasurementDisplayName(
                              item.measurement,
                              item.quantity > 1
                            )}`
                          : undefined
                      }
                      recipe={item.recipe}
                      checked={item.checked}
                      hidden={!checkedVisible && item.checked}
                      onChange={checked =>
                        handleItemChecked(item.name, checked)
                      }
                      onDelete={() => handleItemDelete(item.name)}
                      onClick={() =>
                        history.push(`/grocery-list/item/${item.name}`)
                      }
                    />
                  ))}
              </Card>
            ))}
      </BaseContent>
      <IonFooter>
        <IonToolbar>
          <IonButtons slot="end">
            <Box pr={3}>
              <VisibilityButton
                visible={checkedVisible}
                onChangeVisible={() => setCheckedVisible(!checkedVisible)}
              />
            </Box>
          </IonButtons>
        </IonToolbar>
      </IonFooter>
    </BasePage>
  );
};

export default GroceryList;
