import { IonButton, IonInput, IonItem, IonLabel } from '@ionic/react';
import { Formik } from 'formik';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { Box, Link, Text } from 'rebass';
import { theme } from 'styled-tools';
import BaseContent from '../components/atoms/BaseContent';
import BasePage from '../components/atoms/BasePage';
import SettingsCard from '../components/atoms/SettingsCard';
import { signIn } from '../store/auth/actions';
import styled from '../theme/styled';

const StyledIonItem = styled(IonItem)`
  --background: ${theme('colors.fg1')};
`;

const SignIn: React.FC = () => {
  const history = useHistory();
  const gotoRegister = () => history.push('/registration');

  const dispatch = useDispatch();
  const handleSignIn = (email: string, password: string) =>
    dispatch(signIn.request({ email, password }));

  return (
    <BasePage>
      <BaseContent>
        <Box mt={5} style={{ textAlign: 'center' }}>
          <Text color="text.primary" fontSize={32}>
            Roux
          </Text>
        </Box>
        <Box flex="1 1 auto" pt={5}>
          <SettingsCard>
            <Box p={3}>
              <Formik
                initialValues={{ email: '', password: '' }}
                onSubmit={values => {
                  handleSignIn(values.email, values.password);
                }}
              >
                {({
                  values,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting
                }) => (
                  <form onSubmit={handleSubmit}>
                    <Text color="text.dark" fontSize={24} textAlign="center">
                      Sign In with Email
                    </Text>
                    <StyledIonItem>
                      <IonLabel position="floating">Email Address</IonLabel>
                      <IonInput
                        type="email"
                        name="email"
                        onInput={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                      ></IonInput>
                    </StyledIonItem>
                    <StyledIonItem>
                      <IonLabel position="floating">Password</IonLabel>
                      <IonInput
                        type="password"
                        name="password"
                        onInput={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                      ></IonInput>
                    </StyledIonItem>
                    <Box pt={3} style={{ textAlign: 'right' }}>
                      <IonButton type="submit" disabled={isSubmitting}>
                        Sign In
                      </IonButton>
                    </Box>
                  </form>
                )}
              </Formik>
            </Box>
          </SettingsCard>
          <Box px={3} style={{ textAlign: 'center' }}>
            <Link color="text.primary" onClick={gotoRegister}>
              Don't have an account?
            </Link>
          </Box>
        </Box>
      </BaseContent>
    </BasePage>
  );
};

export default SignIn;
