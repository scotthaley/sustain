import { ItemReorderEventDetail } from '@ionic/core';
import { IonAlert, IonList, IonReorderGroup } from '@ionic/react';
import { addCircleOutline } from 'ionicons/icons';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box } from 'rebass';
import BackButton from '../components/atoms/BackButton';
import BaseContent from '../components/atoms/BaseContent';
import BasePage from '../components/atoms/BasePage';
import SettingsButton from '../components/atoms/SettingsButton';
import SettingsCard from '../components/atoms/SettingsCard';
import SettingsListItem from '../components/atoms/SettingsListItem';
import SettingsTip from '../components/atoms/SettingsTip';
import HeaderBar from '../components/molecules/HeaderBar';
import * as GroceriesActions from '../store/groceries/actions';
import { getAisles } from '../store/groceries/selectors';

interface ISettingsGroceryAislesProps {
  onMenu?: () => void;
}

const SettingsGroceryAisles: React.FC<ISettingsGroceryAislesProps> = ({
  onMenu
}) => {
  const dispatch = useDispatch();
  const [showNewAisleAlert, setShowNewAisleAlert] = React.useState(false);

  const aisles = useSelector(getAisles);

  const reorderList = (event: CustomEvent<ItemReorderEventDetail>) => {
    dispatch(
      GroceriesActions.reorderAisle({
        origIndex: event.detail.from,
        newIndex: event.detail.to
      })
    );
    event.detail.complete();
  };

  const createAisle = (title: string) => {
    dispatch(GroceriesActions.newAisle(title));
  };

  const deleteAisle = (index: number) => {
    dispatch(GroceriesActions.deleteAisle(index));
  };

  return (
    <BasePage>
      <HeaderBar title="Settings" onMenu={onMenu} />
      <BaseContent>
        <BackButton />
        <SettingsCard title="Grocery Aisles">
          <Box>
            <SettingsTip>
              Reorder aisles to match the order at your grocery store.
            </SettingsTip>
            <IonList style={{ margin: '0' }}>
              <IonReorderGroup disabled={false} onIonItemReorder={reorderList}>
                {aisles.map((aisle, i) => (
                  <SettingsListItem key={aisle} onDelete={() => deleteAisle(i)}>
                    {aisle}
                  </SettingsListItem>
                ))}
              </IonReorderGroup>
            </IonList>
          </Box>
          <SettingsButton
            top={true}
            icon={addCircleOutline}
            iconFontSize={24}
            onClick={() => setShowNewAisleAlert(true)}
          >
            New Aisle
          </SettingsButton>
        </SettingsCard>
        <IonAlert
          isOpen={showNewAisleAlert}
          onDidDismiss={() => setShowNewAisleAlert(false)}
          header="New Aisle"
          inputs={[{ name: 'title', type: 'text', placeholder: 'Produce' }]}
          buttons={[
            { text: 'Cancel', role: 'cancel' },
            { text: 'Add Aisle', handler: resp => createAisle(resp.title) }
          ]}
        />
      </BaseContent>
    </BasePage>
  );
};

export default SettingsGroceryAisles;
