import { IonButton, IonInput, IonItem, IonLabel } from '@ionic/react';
import { Formik } from 'formik';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { Box, Link, Text } from 'rebass';
import { theme } from 'styled-tools';
import BaseContent from '../components/atoms/BaseContent';
import BasePage from '../components/atoms/BasePage';
import SettingsCard from '../components/atoms/SettingsCard';
import { signUp } from '../store/auth/actions';
import styled from '../theme/styled';

const StyledIonItem = styled(IonItem)`
  --background: ${theme('colors.fg1')};
`;

const Registration: React.FC = () => {
  const history = useHistory();
  const gotoSignIn = () => history.push('/registration/signin');

  const dispatch = useDispatch();
  const handleSignUp = (email: string, password: string) =>
    dispatch(signUp.request({ email, password }));

  return (
    <BasePage>
      <BaseContent>
        <Box mt={5} style={{ textAlign: 'center' }}>
          <Text color="text.primary" fontSize={32}>
            Roux
          </Text>
        </Box>
        <Box flex="1 1 auto" pt={5}>
          <SettingsCard>
            <Box p={3}>
              <Formik
                initialValues={{ email: '', password: '' }}
                onSubmit={values => {
                  handleSignUp(values.email, values.password);
                }}
              >
                {({
                  values,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit
                }) => (
                  <form onSubmit={handleSubmit}>
                    <Text color="text.dark" fontSize={24} textAlign="center">
                      Signup with Email
                    </Text>
                    <StyledIonItem>
                      <IonLabel position="floating">Email Address</IonLabel>
                      <IonInput
                        type="email"
                        name="email"
                        onInput={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                      ></IonInput>
                    </StyledIonItem>
                    <StyledIonItem>
                      <IonLabel position="floating">Password</IonLabel>
                      <IonInput
                        type="password"
                        name="password"
                        onInput={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                      ></IonInput>
                    </StyledIonItem>
                    <Box pt={3} style={{ textAlign: 'right' }}>
                      <IonButton type="submit">Get Started!</IonButton>
                    </Box>
                  </form>
                )}
              </Formik>
            </Box>
          </SettingsCard>
          <Box px={3} style={{ textAlign: 'center' }}>
            <Link color="text.primary" onClick={gotoSignIn}>
              Already have an account?
            </Link>
          </Box>
        </Box>
      </BaseContent>
    </BasePage>
  );
};

export default Registration;
