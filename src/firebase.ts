import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { Store } from 'redux';
import { storeUser, syncDefaults, syncUserDown } from './store/auth/actions';
import { IDefaults, IRecipe, ISyncUserPayload } from './store/models';
import { syncRecipesDown } from './store/recipes/actions';

const firebaseConfig = {
  apiKey: 'AIzaSyB55BUbbggd868jNaqIiW3ti4q-tlQyIwU',
  authDomain: 'roux-32c5b.firebaseapp.com',
  databaseURL: 'https://roux-32c5b.firebaseio.com',
  projectId: 'roux-32c5b',
  storageBucket: 'roux-32c5b.appspot.com',
  messagingSenderId: '362384230338',
  appId: '1:362384230338:web:6a056dde8bbea65f91385b',
  measurementId: 'G-985TFY7DYR'
};

export const initializeFirebase = (store: Store) => {
  firebase.initializeApp(firebaseConfig);

  firebase.auth().onAuthStateChanged((user: firebase.User | null) => {
    store.dispatch(storeUser(user));
    if (user) {
      setupUserSync(store, user);
      setupRecipesSync(store, user);
      setupDefaultsSync(store);
    }
  });
};

const setupUserSync = (store: Store, user: firebase.User) => {
  firebase
    .firestore()
    .doc(`/users/${user.uid}`)
    .onSnapshot(doc => {
      const data = doc.data();
      if (data) {
        store.dispatch(syncUserDown(data as ISyncUserPayload));
      }
    });
};

const setupRecipesSync = (store: Store, user: firebase.User) => {
  firebase
    .firestore()
    .doc(`/users/${user.uid}`)
    .collection('recipes')
    .onSnapshot(doc => {
      const recipes = doc.docs.map(d => ({ ...d.data(), id: d.id }));
      store.dispatch(syncRecipesDown(recipes as IRecipe[]));
    });
};

const setupDefaultsSync = (store: Store) => {
  firebase
    .firestore()
    .collection('/defaults')
    .onSnapshot(collection => {
      const data = collection.docs.reduce(
        (acc, doc) => ({
          ...acc,
          [doc.id]: doc.data()
        }),
        {}
      );
      if (data) {
        store.dispatch(syncDefaults(data as IDefaults));
      }
    });
};

export const googleAuth = () => {
  const provider = new firebase.auth.GoogleAuthProvider();
  firebase
    .auth()
    .signInWithPopup(provider)
    .then(result => {
      console.log(result);
    });
};
