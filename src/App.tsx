import { IonApp, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';
import '@ionic/react/css/display.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/float-elements.css';
/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/typography.css';
import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import ProtectedRoute, {
  RouteAuthType
} from './components/atoms/ProtectedRoute';
import SideNav from './components/molecules/SideNav';
import GroceryList from './pages/GroceryList';
import GroceryListItemSettings from './pages/GroceryListItemSettings';
import Recipes from './pages/Recipes';
import Registration from './pages/Registration';
import Settings from './pages/Settings';
import SettingsGroceryAisles from './pages/SettingsGroceryAisles';
import SignIn from './pages/SignIn';
/* Theme variables */
import './theme/variables.css';
import { getUser } from './store/auth/selectors';
import { menuController } from '@ionic/core';

const App: React.FC = () => {
  const user = useSelector(getUser);
  const onMenu = () => {
    menuController.open().then(() => {});
  };

  return (
    <IonApp>
      <IonReactRouter>
        {user && (
          <>
            <SideNav contentId="main-content" />
            <IonRouterOutlet id="main-content">
              <ProtectedRoute
                path="/grocery-list"
                component={() => <GroceryList onMenu={onMenu} />}
                exact={true}
              />
              <ProtectedRoute
                path="/grocery-list/item/:name"
                component={() => <GroceryListItemSettings onMenu={onMenu} />}
              />
              <ProtectedRoute
                path="/recipes"
                component={() => <Recipes onMenu={onMenu} />}
              />
              <ProtectedRoute
                path="/settings"
                exact={true}
                component={() => <Settings onMenu={onMenu} />}
              />
              <ProtectedRoute
                path="/settings/aisles"
                component={() => <SettingsGroceryAisles onMenu={onMenu} />}
              />
              <ProtectedRoute
                authType={RouteAuthType.UnauthOnly}
                path="/registration/signin"
                component={SignIn}
              />
              <ProtectedRoute
                authType={RouteAuthType.UnauthOnly}
                path="/registration"
                component={Registration}
              />
              <Redirect exact from="/" to="/registration" />
            </IonRouterOutlet>
          </>
        )}
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
