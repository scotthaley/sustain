import { InputChangeEventDetail } from '@ionic/core';
import React from 'react';
import { useSelector } from 'react-redux';
import { Box } from 'rebass';
import { getAisles } from '../../store/groceries/selectors';
import CategoryPicker from '../atoms/CategoryPicker';
import SmallInput from '../atoms/SmallInput';
import TopDrawer from '../atoms/TopDrawer';

interface IGroceryListDrawerProps {
  expanded: boolean;
  requestedAisle?: string;
  itemText: string;
  onItemText: (text: string) => void;
  onAisle: (category: string) => void;
  onAisleOverride: (override: boolean) => void;
  onItemAdd: () => void;
}

const GroceryListDrawer: React.FC<IGroceryListDrawerProps> = ({
  expanded,
  requestedAisle,
  itemText,
  onItemText,
  onAisle,
  onAisleOverride,
  onItemAdd
}) => {
  const aisles = ['Uncategorized', ...useSelector(getAisles)];
  const [selectedAisle, setSelectedAisle] = React.useState(0);
  const [overrideAisle, setOverrideAisle] = React.useState(false);

  const handleInputChange = (event: CustomEvent<InputChangeEventDetail>) => {
    const newText = event.detail.value || '';
    onItemText(newText);
    setOverrideAisle(false);
    onAisleOverride(false);
  };

  const handleCategoryChange = (id: number) => {
    onAisle(aisles[id]);
    setOverrideAisle(true);
    onAisleOverride(true);
    setSelectedAisle(id);
  };

  if (!overrideAisle) {
    const aisleIndex = aisles.findIndex(a => a === requestedAisle);
    if (aisleIndex) {
      handleCategoryChange(aisleIndex);
    }
  }

  const handleKeyPress = (event: React.KeyboardEvent) => {
    if (event.keyCode === 13) {
      onItemAdd();
    }
  };

  return (
    <TopDrawer height={120} expanded={expanded}>
      <SmallInput
        type="text"
        placeholder="Add something..."
        value={itemText}
        onIonChange={handleInputChange}
        onKeyDown={handleKeyPress}
        clearInput={true}
      />
      {aisles.length > 0 && (
        <Box mt={3}>
          <CategoryPicker
            label="Choose an Aisle"
            selectedCategoryId={selectedAisle}
            categories={aisles.map((a, i) => ({ label: a, id: i }))}
            onSelect={handleCategoryChange}
          />
        </Box>
      )}
    </TopDrawer>
  );
};

export default GroceryListDrawer;
