import React from 'react';
import { Box, Flex, Text } from 'rebass';
import styled from '../../theme/styled';

interface IRecipeIconProps {
  name: string;
  icon: string;
}

const ContainerBox = styled(Box)`
  display: inline-block;
  width: 130px;
  height: 160px;
  overflow: hidden;
`;

const IconBox = styled(Box)<{ img: string }>`
  border-radius: 20px;
  overflow: hidden;
  width: 110px;
  height: 110px;
  background-image: url(${props => props.img});
  background-size: cover;
  background-position: center;
`;

const RecipeIcon: React.FC<IRecipeIconProps> = ({ name, icon }) => {
  return (
    <ContainerBox mb={3}>
      <Flex alignItems="center" flexDirection="column">
        <IconBox img={icon} />
        <Box mt={2} style={{ textAlign: 'center' }}>
          <Text color="text.primary" fontSize={1}>
            {name}
          </Text>
        </Box>
      </Flex>
    </ContainerBox>
  );
};

export default RecipeIcon;
