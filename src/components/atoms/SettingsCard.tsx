import React from 'react';
import { Box, Text } from 'rebass';

interface ISettingsCardProps {
  title?: string;
}

const SettingsCard: React.FC<ISettingsCardProps> = ({ title, children }) => {
  return (
    <Box m={3}>
      {title && <Text color="text.light">{title}</Text>}
      <Box mt={2} bg="fg1" variant="card">
        {children}
      </Box>
    </Box>
  );
};

export default SettingsCard;
