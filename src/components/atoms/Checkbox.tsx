import styled from '@emotion/styled';
import React from 'react';
import { Box } from 'rebass';
import { theme } from 'styled-tools';

interface ICheckboxProps {
  checked: boolean;
  onChange: (checked: boolean) => void;
}

const CheckboxContainer = styled(Box)`
  position: relative;
  border-radius: 8px;
  width: 30px;
  height: 30px;
  border: solid 3px ${theme('colors.fg1')};
  overflow: hidden;
`;

const CheckboxCheck = styled(Box)<{ checked: boolean }>`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  border-radius: 50%;
  background-color: ${theme('colors.primary')};
  width: ${props => (props.checked ? '150%' : '0%')};
  height: ${props => (props.checked ? '150%' : '0%')};
  transition: width 0.2s ease-in, height 0.2s ease-in;
`;

const Checkbox: React.FC<ICheckboxProps> = ({ checked, onChange }) => {
  const handleClick = (e: React.MouseEvent) => {
    e.stopPropagation();
    onChange(!checked);
  };
  return (
    <CheckboxContainer onClick={handleClick}>
      <CheckboxCheck checked={checked} />
    </CheckboxContainer>
  );
};

export default Checkbox;
