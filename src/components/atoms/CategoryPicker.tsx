import styled from '@emotion/styled';
import React from 'react';
import { Button, Flex, Text } from 'rebass';
import { theme } from 'styled-tools';
import { ICategory } from '../../store/models';

interface ICategoryPickerProps {
  label?: string;
  selectedCategoryId: number;
  categories: ICategory[];
  onSelect: (categoryId: number) => void;
}

const CategoryPickerContainer = styled.div`
  position: relative;
  height: 25px;
  width: 175px;
  z-index: ${theme('zIndices.categorypicker')};
`;

const CategoryPickerScrim = styled.div<{ expanded: boolean }>`
  position: absolute;
  left: 50%;
  top: 50%;
  width: ${props => (props.expanded ? '150vh' : '0')};
  height: ${props => (props.expanded ? '150vh' : '0')};
  border-radius: 50%;
  background-color: ${theme('colors.bg1')};
  opacity: 0.97;
  transform: translate(-50%, -50%);
  transition: width 0.5s ease-out, height 0.5s ease-out;
`;

const CategoryOptionsContainer = styled(Flex)<{ expanded: boolean }>`
  opacity: ${props => (props.expanded ? 1 : 0)};
  transition: opacity 0.5s ease-out;
  pointer-events: ${props => (props.expanded ? 'all' : 'none')};
`;

const LabelContainer = styled.div<{ expanded: boolean }>`
  position: absolute;
  top: -40px;
  opacity: ${props => (props.expanded ? 1 : 0)};
  transition: opacity 0.5s ease-out;
  width: 100%;
  display: flex;
  justify-content: center;
`;

const CategoryPickerButton = styled(Button)`
  position: absolute;
  border: 0.5px solid ${theme('colors.bg3')};
  border-radius: 8px;
  background-color: transparent;
  padding: 2px;
  width: 100%;
`;

const CategoryPicker: React.FC<ICategoryPickerProps> = ({
  selectedCategoryId,
  categories,
  label,
  onSelect
}) => {
  const [expanded, setExpanded] = React.useState(false);
  const selectedCategory = () =>
    categories.find(c => c.id === selectedCategoryId) || categories[0];

  const otherCategories = () =>
    categories.filter(c => c.id !== selectedCategoryId);

  const selectCategory = (id: number) => () => {
    setExpanded(false);
    onSelect(id);
  };

  return (
    <CategoryPickerContainer>
      <CategoryPickerScrim
        expanded={expanded}
        onClick={() => setExpanded(false)}
      ></CategoryPickerScrim>
      {label && (
        <LabelContainer expanded={expanded}>
          <Text fontSize={18} color="text.primary">
            {label}
          </Text>
        </LabelContainer>
      )}
      <CategoryPickerButton onClick={() => setExpanded(true)}>
        <Text fontSize={16} fontWeight={200}>
          {selectedCategory().label}
        </Text>
      </CategoryPickerButton>
      <CategoryOptionsContainer
        expanded={expanded}
        flexDirection="column"
        alignItems="center"
        style={{ position: 'absolute', top: '35px', width: '100%' }}
      >
        {otherCategories().map((c, i) => (
          <Text
            key={c.id}
            color="text.primary"
            fontSize={16}
            fontWeight={100}
            mt={2}
            style={{
              position: 'absolute',
              transform: `translateY(${i * 30 * (expanded ? 1 : 0)}px)`,
              transition: 'transform 0.5s ease-out'
            }}
            onClick={selectCategory(c.id)}
          >
            {c.label}
          </Text>
        ))}
      </CategoryOptionsContainer>
    </CategoryPickerContainer>
  );
};

export default CategoryPicker;
