import { IonIcon } from '@ionic/react';
import { arrowBack } from 'ionicons/icons';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { Flex, Text } from 'rebass';
import { theme } from 'styled-tools';
import styled from '../../theme/styled';

const BackButton: React.FC = () => {
  const history = useHistory();

  const goBack = () => {
    history.goBack();
  };

  const StyledButton = styled.button`
    background-color: transparent;
    margin-top: ${theme('space.2')}px;
    text-align: left;
    height: 32px;
  `;

  return (
    <StyledButton onClick={goBack}>
      <Text color="text.primary">
        <Flex alignItems="center" fontSize={16}>
          <IonIcon icon={arrowBack} size="large"></IonIcon>
          Back
        </Flex>
      </Text>
    </StyledButton>
  );
};

export default BackButton;
