import React from 'react';
import { Box, Text } from 'rebass';

const Logo: React.FC = () => {
  return (
    <Box p={3} pt={5} bg="primary">
      <Text fontSize={30} fontWeight={100} color="text.primary">
        Sustain
      </Text>
    </Box>
  );
};

export default Logo;
