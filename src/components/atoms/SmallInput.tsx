import { IonInput } from '@ionic/react';
import { theme } from 'styled-tools';
import styled from '../../theme/styled';

const SmallInput = styled(IonInput)`
  width: 70%;
  background-color: ${theme('colors.fg1')};
  border-radius: 8px;
  flex: 0 0 auto;
  padding: 0 ${theme('space.2')}px !important;
`;

export default SmallInput;
