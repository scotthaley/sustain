import styled from '@emotion/styled';
import React from 'react';
import { NavLink } from 'react-router-dom';
import { Text } from 'rebass';
import { theme } from 'styled-tools';

interface INavItemProps {
  route: string;
  onClick?: () => void;
}

const NavItemLink = styled(NavLink)`
  display: block;
  text-decoration: none;
  padding: ${theme('space.2')}px;
  padding-left: ${theme('space.3')}px;

  &.active {
    background-color: ${theme('colors.bg2')};
  }
`;

const NavItem: React.FC<INavItemProps> = ({ route, children, onClick }) => {
  return (
    <NavItemLink to={route} onClick={onClick}>
      <Text color="text.primary" fontWeight={100} fontSize={18}>
        {children}
      </Text>
    </NavItemLink>
  );
};

export default NavItem;
