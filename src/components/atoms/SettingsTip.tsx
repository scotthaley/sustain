import { IonIcon } from '@ionic/react';
import { school } from 'ionicons/icons';
import React from 'react';
import { Box, Flex, Text } from 'rebass';

const SettingsTip: React.FC = ({ children }) => {
  return (
    <Flex p={3} alignItems="center" variant="bottomBorder">
      <Box mr={3} width={45}>
        <IonIcon icon={school} size="large" />
      </Box>
      <Text>{children}</Text>
    </Flex>
  );
};

export default SettingsTip;
