import {
  IonIcon,
  IonItem,
  IonItemOption,
  IonItemOptions,
  IonItemSliding,
  IonLabel,
  IonReorder
} from '@ionic/react';
import { trash } from 'ionicons/icons';
import React from 'react';
import { Text } from 'rebass';
import { theme } from 'styled-tools';
import styled from '../../theme/styled';

interface ISettingsListItemProps {
  onDelete: () => void;
}

const StyledIonItem = styled(IonItem)`
  --background: ${theme('colors.fg1')};
`;

const SettingsListItem: React.FC<ISettingsListItemProps> = ({
  children,
  onDelete
}) => {
  return (
    <IonItemSliding>
      <StyledIonItem lines="full">
        <IonLabel>
          <Text color="text.dark">{children}</Text>
        </IonLabel>
        <IonReorder></IonReorder>
      </StyledIonItem>
      <IonItemOptions side="end">
        <IonItemOption color="danger" onClick={onDelete}>
          <IonIcon icon={trash} size="large"></IonIcon>
        </IonItemOption>
      </IonItemOptions>
    </IonItemSliding>
  );
};

export default SettingsListItem;
