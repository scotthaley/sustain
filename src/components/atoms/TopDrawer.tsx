import React from 'react';
import { Flex } from 'rebass';
import styled from '../../theme/styled';

interface ITopDrawerProps {
  height: number;
  expanded: boolean;
}

const StyledFlex = styled(Flex)<{ height: number; expanded: boolean }>`
  overflow: ${props => (props.expanded ? 'visible' : 'hidden')};
  height: ${props => props.height}px;
  transition: height 0.2s ease-in;
`;

const TopDrawer: React.FC<ITopDrawerProps> = ({
  children,
  height,
  expanded
}) => {
  return (
    <StyledFlex
      flexDirection="column"
      bg="bg2"
      alignItems="center"
      justifyContent="center"
      height={expanded ? height : 0}
      expanded={expanded}
    >
      {children}
    </StyledFlex>
  );
};

export default TopDrawer;
