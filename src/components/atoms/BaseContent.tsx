import { IonContent } from '@ionic/react';
import styled from 'styled-components';
import { theme } from 'styled-tools';

export default styled(IonContent)`
  font-family: Nunito;
  --padding-bottom: 40px;
  --color: ${theme('colors.text.primary')};
`;
