import { Text } from 'rebass';
import styled from '../../theme/styled';

const OverflowText = styled(Text)`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export default OverflowText;
