import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route, RouteProps } from 'react-router';
import { getUser } from '../../store/auth/selectors';

export enum RouteAuthType {
  AuthOnly,
  UnauthOnly
}

export interface IProtectedRouteProps extends RouteProps {
  authType?: RouteAuthType;
}

const ProtectedRoute: React.FC<IProtectedRouteProps> = ({
  authType = RouteAuthType.AuthOnly,
  ...rest
}) => {
  const user = useSelector(getUser);
  const isAuthenticated = user !== null;

  if (authType === RouteAuthType.AuthOnly) {
    if (isAuthenticated) {
      return <Route {...rest} />;
    } else {
      return <Redirect to="/" />;
    }
  } else {
    if (isAuthenticated) {
      return <Redirect to="/grocery-list" />;
    } else {
      return <Route {...rest} />;
    }
  }
};

export default ProtectedRoute;
