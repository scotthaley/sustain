import { IonIcon } from '@ionic/react';
import React from 'react';
import { Flex, Text } from 'rebass';
import { theme } from 'styled-tools';
import styled from '../../theme/styled';

interface ISettingsButtonProps {
  theme?: string;
  top?: boolean;
  icon?: string;
  iconFontSize?: number;
  onClick?: () => void;
}

/* eslint-disable no-unexpected-multiline*/
const StyledSettingsButton = styled.button<{
  top?: boolean;
  buttonTheme?: string;
}>`
  margin: 0;
  padding: ${theme('space.3')}px;
  background-color: ${props =>
    props.buttonTheme === 'create'
      ? props.theme.colors.fg2
      : props.theme.colors.fg1};
  border: none;
  border-top: ${props =>
    props.top ? '' : `1px solid ${props.theme.colors.bg3}`};
  width: 100%;
  transition: background-color 0.3s ease;

  &:active {
    background-color: ${theme('colors.bg3')};
  }
`;
/* eslint-enable */

const SettingsButton: React.FC<ISettingsButtonProps> = ({
  children,
  top,
  icon,
  iconFontSize,
  theme,
  onClick
}) => {
  return (
    <StyledSettingsButton top={top} buttonTheme={theme} onClick={onClick}>
      <Flex
        justifyContent={icon ? 'space-between' : 'center'}
        alignItems="center"
      >
        <Text color="text.dark" fontSize={18}>
          {children}
        </Text>
        {icon && (
          <Text color="text.light">
            <IonIcon
              icon={icon}
              style={{ fontSize: `${iconFontSize || 16}px` }}
            />
          </Text>
        )}
      </Flex>
    </StyledSettingsButton>
  );
};

export default SettingsButton;
