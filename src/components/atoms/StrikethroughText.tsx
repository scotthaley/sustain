import React from 'react';
import { TextProps } from 'rebass';
import { theme } from 'styled-tools';
import styled from '../../theme/styled';
import OverflowText from './OverflowText';

interface IStrikethroughTextProps extends TextProps {
  strikethrough?: boolean;
}

const StyledTextContainer = styled.div<{ strikethrough: boolean }>`
  position: absolute;
  top: 0;
  left: 0;
  width: ${props => (props.strikethrough ? '100%' : '0%')};
  transition: width 0.2s ease-in;
`;

const StyledText = styled(OverflowText)`
  text-decoration: line-through;
  color: ${theme('colors.bg3')};
`;

const StrikethroughText: React.FC<IStrikethroughTextProps> = ({
  children,
  strikethrough = false,
  ...rest
}) => {
  return (
    <OverflowText {...rest} style={{ ...rest.style, position: 'relative' }}>
      <StyledTextContainer strikethrough={strikethrough}>
        <StyledText>{children}</StyledText>
      </StyledTextContainer>
      {children}
    </OverflowText>
  );
};

export default StrikethroughText;
