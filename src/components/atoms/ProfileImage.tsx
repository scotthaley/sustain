import styled from '@emotion/styled';
import React from 'react';
import Gravatar from 'react-gravatar';
import { theme } from 'styled-tools';

export enum ProfileImageSize {
  Small = 40,
  Medium = 60,
  Large = 80
}

interface IProfileImageProps {
  email: string;
  size?: ProfileImageSize;
}

const StyledGravatar = styled(Gravatar)`
  border-radius: 50%;
  box-shadow: ${theme('shadows.card')};
`;

const ProfileImage: React.FC<IProfileImageProps> = ({ email, size }) => {
  return (
    <StyledGravatar
      email={email}
      size={size || 40}
      protocol="https://"
    ></StyledGravatar>
  );
};

export default ProfileImage;
