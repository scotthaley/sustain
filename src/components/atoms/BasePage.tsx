import styled from '@emotion/styled';
import { IonPage } from '@ionic/react';
import { theme } from 'styled-tools';

export default styled(IonPage)`
  background-color: ${theme('colors.bg1')};
  --ion-background-color: ${theme('colors.bg1')};
  --ion-padding: 0;
  padding-top: ${theme('space.3')}px;
`;
