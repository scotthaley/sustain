import { IonButton, IonIcon } from '@ionic/react';
import { eye, eyeOff } from 'ionicons/icons';
import React from 'react';
import styled from '../../theme/styled';

interface IVisibilityButtonProps {
  visible: boolean;
  onChangeVisible: () => void;
}

const StyledIonButton = styled(IonButton)`
  height: 50px !important;
`;

const VisibilityButton: React.FC<IVisibilityButtonProps> = ({
  visible,
  onChangeVisible
}) => {
  return (
    <StyledIonButton onClick={onChangeVisible}>
      <IonIcon slot="icon-only" icon={visible ? eye : eyeOff} />
    </StyledIonButton>
  );
};

export default VisibilityButton;
