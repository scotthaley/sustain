import styled from '@emotion/styled';
import React from 'react';
import { Flex, Text } from 'rebass';
import ProfileImage from '../atoms/ProfileImage';

const EmailText = styled(Text)`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const ProfileBar: React.FC = () => {
  return (
    <Flex p={3} alignItems="center">
      <ProfileImage email="williamscotthaley@gmail.com"></ProfileImage>
      <EmailText color="text.primary" fontSize={14} fontWeight={100} ml={2}>
        williamscotthaley@gmail.com
      </EmailText>
    </Flex>
  );
};

export default ProfileBar;
