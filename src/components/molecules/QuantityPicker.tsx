import { IonPicker } from '@ionic/react';
import React from 'react';
import { Box, Text } from 'rebass';
import { theme } from 'styled-tools';
import { Measurement } from '../../store/models';
import styled from '../../theme/styled';
import { getMeasurementDisplayName } from '../../utils/measurement-utils';

interface IQuantityPickerProps {
  qty: number;
  measurement: Measurement;
  onChange: (qty: number, measurment: Measurement) => void;
}

const StyledButton = styled.button`
  background-color: transparent;
  border: 1px solid ${theme('colors.bg3')};
  border-radius: ${theme('space.2')}px;
`;

const QuantityPicker: React.FC<IQuantityPickerProps> = ({
  qty,
  measurement,
  onChange
}) => {
  const [pickerOpen, setPickerOpen] = React.useState(false);

  const pickerButtons = [
    {
      text: 'Cancel'
    },
    {
      text: 'Update',
      handler: ({
        Amount,
        Measurement
      }: {
        Amount: { value: number };
        Measurement: { value: Measurement };
      }) => onChange(Amount.value, Measurement.value)
    }
  ];

  const pickerColumns = [
    {
      name: 'Amount',
      options: [...Array(200)].map((a, i) => ({
        text: (i + 1).toString(),
        value: i + 1
      })),
      selectedIndex: qty - 1
    },
    {
      name: 'Measurement',
      options: Object.keys(Measurement)
        .filter(m => !isNaN(Number(m)))
        .map(m => ({
          text: getMeasurementDisplayName(
            (Number(m) as any) as Measurement,
            false,
            true
          ),
          value: (Number(m) as any) as Measurement
        })),
      selectedIndex: Number(measurement)
    }
  ];

  return (
    <>
      <StyledButton type="button" onClick={() => setPickerOpen(true)}>
        <Box p={2}>
          <Text color="text.dark">
            {qty} {getMeasurementDisplayName(measurement, qty > 1)}
          </Text>
        </Box>
      </StyledButton>
      <IonPicker
        columns={pickerColumns}
        buttons={pickerButtons}
        isOpen={pickerOpen}
        onDidDismiss={() => setPickerOpen(false)}
      />
    </>
  );
};

export default QuantityPicker;
