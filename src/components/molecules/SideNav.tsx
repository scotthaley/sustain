import styled from '@emotion/styled';
import React from 'react';
import { Flex } from 'rebass';
import Logo from '../atoms/Logo';
import NavItem from '../atoms/NavItem';
import ProfileBar from './ProfileBar';
import { IonMenu } from '@ionic/react';
import { menuController } from '@ionic/core';

interface ISideNavProps {
  contentId: string;
}

const SideNavContainer = styled(Flex)<{ expanded: boolean }>`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

const SideNav: React.FC<ISideNavProps> = ({ contentId }) => {
  const onExit = () => {
    menuController.close().then(() => {});
  };

  return (
    <IonMenu side="start" contentId={contentId}>
      <SideNavContainer flexDirection="column" expanded={true}>
        <Flex bg="bg1" flex="1 1 auto" flexDirection="column">
          <Logo />
          <Flex mt={4} flexDirection="column" flex="1 1 auto">
            <NavItem route="/grocery-list" onClick={onExit}>
              Grocery List
            </NavItem>
            <NavItem route="/recipes" onClick={onExit}>
              Recipes
            </NavItem>
            <NavItem route="/settings" onClick={onExit}>
              Settings
            </NavItem>
          </Flex>
          <ProfileBar />
        </Flex>
      </SideNavContainer>
    </IonMenu>
  );
};

export default SideNav;
