import styled from '@emotion/styled';
import React from 'react';
import { Box, Text } from 'rebass';
import { theme } from 'styled-tools';

const CardHeader = styled(Box)`
  background-color: ${theme('colors.primary')};
  color: ${theme('colors.text.primary')};
  padding: ${theme('space.2')}px 0;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
`;

interface ICardProps {
  title: string;
  hidden?: boolean;
  maxHeight?: number;
}

const StyledBox = styled(Box)<{ hiddenCard: boolean; maxHeight?: number }>`
  transform: ${props =>
    props.hiddenCard ? 'translateX(-120%)' : 'translateX(0%)'};
  max-height: ${props =>
    props.hiddenCard ? '0' : props.maxHeight || '9999'}px;
  margin: ${props =>
    props.hiddenCard ? '0' : (props.theme as any).space[4]}px;
  margin-bottom: 0;
  transition: transform 0.3s ${props => (props.hiddenCard ? '1s' : '0s')}
      ease-in,
    max-height 0.3s ${props => (props.hiddenCard ? '1s' : '0s')} ease-in,
    margin 0.3s ${props => (props.hiddenCard ? '1s' : '0s')} ease-in;
`;

const Card: React.FC<ICardProps> = ({
  title,
  hidden = false,
  maxHeight,
  children
}) => {
  return (
    <StyledBox variant="card" hiddenCard={hidden} maxHeight={maxHeight}>
      <CardHeader>
        <Text fontSize={20} fontWeight={600} textAlign="center">
          {title}
        </Text>
      </CardHeader>
      <Box p={2}>{children}</Box>
    </StyledBox>
  );
};

export default Card;
