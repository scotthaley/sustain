import React from 'react';
import { Flex, Text } from 'rebass';
import hamburger from '../../assets/icons/hamburger.png';

interface IHeaderBarProps {
  title: string;
  rightIcon?: string;
  onMenu?: () => void;
  onRightAction?: () => void;
}

const HeaderBar: React.FC<IHeaderBarProps> = ({
  title,
  rightIcon,
  onMenu,
  onRightAction
}) => {
  return (
    <Flex
      justifyContent="space-between"
      alignItems="center"
      bg="bg2"
      py={2}
      px={3}
      variant="floating"
      mt={45}
      style={{ zIndex: 99 }}
    >
      <Flex width={30} onClick={onMenu}>
        <img src={hamburger} height="15" alt="menu" />
      </Flex>
      <Text color="text.primary" fontSize={20} fontWeight={200}>
        {title}
      </Text>
      <Flex width={30} justifyContent="flex-end" onClick={onRightAction}>
        {rightIcon && <img src={rightIcon} height="20" alt="other" />}
      </Flex>
    </Flex>
  );
};

export default HeaderBar;
