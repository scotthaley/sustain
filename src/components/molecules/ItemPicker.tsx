import React from 'react';
import { Box, Flex, Text } from 'rebass';
import { theme } from 'styled-tools';
import plus from '../../assets/icons/plus.png';
import { IGroceryItem } from '../../store/models';
import styled from '../../theme/styled';
import { getMeasurementDisplayName } from '../../utils/measurement-utils';
import OverflowText from '../atoms/OverflowText';

interface IItemPickerProps {
  items: IGroceryItem[];
  onItemSelect: (text: string, aisle: string) => void;
  onItemAdd: (item: IGroceryItem) => void;
}

const ItemPickerContainer = styled(Box)`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: ${theme('colors.bg1')};
  z-index: ${theme('zIndices.itempicker')};
`;

const ItemBox = styled(Flex)`
  border-bottom: 0.5px solid ${theme('colors.bg2')};
`;

const ItemPicker: React.FC<IItemPickerProps> = ({
  items,
  onItemSelect,
  onItemAdd
}) => {
  return (
    <ItemPickerContainer>
      {items.map((item, i) => (
        <ItemBox key={i} alignItems="center">
          <Flex
            flex="1 1 auto"
            onClick={() => onItemSelect(item.name, item.aisle)}
            p={3}
            alignItems="center"
          >
            <Text color="text.primary">{item.name}</Text>
            <OverflowText color="text.secondary" ml={3} flex="0 0 auto">
              {item.quantity}
            </OverflowText>
            <OverflowText color="text.secondary" ml={1} flex="0 0 auto">
              {getMeasurementDisplayName(item.measurement, item.quantity > 1)}
            </OverflowText>
            <OverflowText color="text.light" ml={3} flex="1 0 60px">
              {item.aisle}
            </OverflowText>
          </Flex>
          <Box p={3} flex="0 0 auto" onClick={() => onItemAdd(item)}>
            <img src={plus} width="20" height="20" alt="plus icon" />
          </Box>
        </ItemBox>
      ))}
    </ItemPickerContainer>
  );
};

export default ItemPicker;
