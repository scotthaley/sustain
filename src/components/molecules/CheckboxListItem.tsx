import {
  IonIcon,
  IonItem,
  IonItemOption,
  IonItemOptions,
  IonItemSliding,
  IonLabel
} from '@ionic/react';
import { trash } from 'ionicons/icons';
import React from 'react';
import { Box, Flex, Text } from 'rebass';
import { theme } from 'styled-tools';
import styled from '../../theme/styled';
import Checkbox from '../atoms/Checkbox';
import StrikethroughText from '../atoms/StrikethroughText';

interface ICheckboxListItemProps {
  checked: boolean;
  onChange: (checked: boolean) => void;
  onDelete: () => void;
  onClick: () => void;
  title: string;
  qty?: string;
  recipe?: string;
  hidden?: boolean;
}

const StyledIonItem = styled(IonItem)<{ hiddenItem: boolean }>`
  --background: ${theme('colors.bg2')};
  max-height: ${props => (props.hiddenItem ? '0px' : '50px')};
  opacity: ${props => (props.hiddenItem ? '0.4' : '1')};
  transition: max-height 0.3s ease-in
      ${props => (props.hiddenItem ? '1s' : '0')},
    opacity 0.3s ease-in;
`;

const StyledIonLabel = styled(IonLabel)`
  margin-top: ${theme('space.1')}px;
  margin-bottom: ${theme('space.1')}px;
`;

const StyledIonItemOptions = styled(IonItemOptions)`
  border-bottom: none;
`;

const CheckboxListItem: React.FC<ICheckboxListItemProps> = ({
  checked,
  onChange,
  onDelete,
  onClick,
  title,
  qty,
  recipe,
  hidden = false
}) => {
  return (
    <IonItemSliding onClick={onClick}>
      <StyledIonItem lines="none" hiddenItem={hidden}>
        <StyledIonLabel>
          <Flex alignItems="center">
            <Box mr={3} style={{ minWidth: '30px' }}>
              <Checkbox checked={checked} onChange={onChange} />
            </Box>
            <Flex flexDirection="column">
              <Box>
                <StrikethroughText
                  strikethrough={checked}
                  fontSize={16}
                  fontWeight={600}
                  color="text.primary"
                  style={{ textOverflow: 'ellipsis', overflow: 'hidden' }}
                >
                  {title}
                </StrikethroughText>
              </Box>
              <Flex>
                {qty && (
                  <Text
                    fontSize={14}
                    fontWeight={200}
                    color="text.secondary"
                    mr={1}
                  >
                    {qty}
                  </Text>
                )}
                {recipe && (
                  <Text
                    fontSize={14}
                    fontWeight={200}
                    color="text.light"
                    style={{ textOverflow: 'ellipses', overflow: 'hidden' }}
                  >
                    {qty ? '- ' : ''}for <i>{recipe}</i>
                  </Text>
                )}
              </Flex>
            </Flex>
          </Flex>
        </StyledIonLabel>
      </StyledIonItem>
      <StyledIonItemOptions side="end">
        <IonItemOption
          color="danger"
          onClick={e => {
            onDelete();
            e.stopPropagation();
          }}
        >
          <IonIcon icon={trash} size="large" />
        </IonItemOption>
      </StyledIonItemOptions>
    </IonItemSliding>
  );
};

export default CheckboxListItem;
