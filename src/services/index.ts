import * as auth from './auth-service';

export default {
  api: {
    auth
  }
};
