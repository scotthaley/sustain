import * as firebase from 'firebase/app';
import { RootState } from 'typesafe-actions';

export const emailAuth = (email: string, password: string) =>
  firebase.auth().signInWithEmailAndPassword(email, password);

export const emailSignUp = (email: string, password: string) =>
  firebase.auth().createUserWithEmailAndPassword(email, password);

export const createUserDoc = (uid: string, email: string) =>
  firebase
    .firestore()
    .doc(`/users/${uid}`)
    .set({ email });

export const sync = (state: RootState) => {
  if (!state.auth.user) return;
  const uid = state.auth.user.uid;
  const userDoc = firebase.firestore().doc(`/users/${uid}`);
  userDoc.update({
    'groceries.aisles': state.groceries.aisles,
    'groceries.items': state.groceries.items,
    'defaults.groceryItems': state.auth.userDefaults.groceryItems
  });
};
